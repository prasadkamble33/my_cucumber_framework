package Driver_Package;



import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import utility_common_method.Excel_data_exctracor_2;

public class Dynamic_Driver_Class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		ArrayList<String> TC_Executor = Excel_data_exctracor_2.Excel_Data_Reader("Test_Data","Test_Cases","TC_Name");
	    System.out.println(TC_Executor);
	    
	    int count = TC_Executor.size();
	    for(int i=1; i<count; i++) {
	    String TC_Name = TC_Executor.get(i); 	
	    System.out.println(TC_Name);
	    
	    // Call the test class on runtime by using java.lang.reflect.package.
	    Class<?> Test_Class = Class.forName("Test_All_API_Executor."+ TC_Name);
	    
	    // Call the Executor Method belonging to test class to  captured in varible  TC_Name by using java.lang.reflect.Method class.
	    Method Executor_Method = Test_Class.getDeclaredMethod("Executor");
	    
	    // Set Accessibility the method of true.
	    Executor_Method.setAccessible(true);
	    
	    // Create Object Instance of Test Class to captured variable in name testclass.
	    Object Instance_Of_Test_Case = Test_Class.getDeclaredConstructor().newInstance();
	    
	    // Execute test script class to fetched variable Test_Class.
	    Executor_Method.invoke(Instance_Of_Test_Case);
	    
	    
	    }
	    
		}
	}


