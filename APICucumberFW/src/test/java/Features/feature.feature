Feature: Trigger API

@Post_API_TestScript
Scenario: Trigger Post API request with valid request parameters
    Given Enter NAME and JOB in Post request body
     When Send the requestwith Post payload
     Then Validate Post status code
     Then Validate Post response body 
      
@Put_API_TestScript 
Scenario: Trigger Put API request with valid request parameters
    Given Input NAME and JOB in Put request body
     When Send request with valid Put payload
     Then Validate the Put Status Code
     Then Validate Put Response Body

@Patch_API_TestScript     
Scenario: Trigger Patch API request with valid request parameters
    Given Enter NAME and JOB in Patch request body
     When Send request with valid Patch payload
     Then Validate the Patch Status Code
     Then Validate Patch Response Body
  
@Get_API_TestScript     
Scenario: Trigger Get API request 
    Given Enter endpoint
     When Send request with valid Get payload
     Then Validate the Get Status Code
     Then Validate Get Response Body
