package Step_Defination;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Get_Step_Defination extends Common_Method_Handle_API {

	static int StatusCode;
	static String responseBody;
	static String endpoint;

	@Given("Enter endpoint")
	public void enter_endpoint() {
		endpoint = get_endpoint.get_endpoint();

	}

	@When("Send request with valid Get payload")
	public void send_request_with_valid_get_payload() {
		StatusCode = Common_Method_Handle_API.get_statuscode(endpoint);
		responseBody = Common_Method_Handle_API.get_responseBody(endpoint);
		System.out.println("Get API Response body :" + responseBody);
	}

	@Then("Validate the Get Status Code")
	public void validate_the_get_status_code() {
		Assert.assertEquals(StatusCode, 200);

	}

	@Then("Validate Get Response Body")
	public void validate_get_response_body() {
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_dataarray = jsp_res.getString("data");
		JSONObject res_array = new JSONObject(responseBody);
		JSONArray dataarray = res_array.getJSONArray("data");
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");

			int expected_id[] = { 7, 8, 9, 10, 11, 12 };
			String expected_first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
			String expected_last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
			String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in",
					"tobias.funke@reqres.in", "byron.fields@reqres.in", "george.edwards@reqres.in",
					"rachel.howell@reqres.in" };

			int exp_id = expected_id[i];
			String exp_first_name = expected_first_name[i];
			String exp_last_name = expected_last_name[i];
			String exp_email = expected_email[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
			Assert.assertEquals(res_email, exp_email);

		}
		System.out.println("Get API : Response Body Validation Successfull");

	}
}
