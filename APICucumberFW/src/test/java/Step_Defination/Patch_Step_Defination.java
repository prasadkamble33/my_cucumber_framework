package Step_Defination;

import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.patch_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;

public class Patch_Step_Defination extends Common_Method_Handle_API {
	static String requestBody;
	static int StatusCode;
	static String responseBody;
	static String endpoint;

	@Given("Enter NAME and JOB in Patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		endpoint = patch_endpoint.patch_endpoint_TC3();
		requestBody = Patch_request_repository.patch_request_TC3();
	}

	@When("Send request with valid Patch payload")
	public void send_request_with_valid_patch_payload() {
		StatusCode = Common_Method_Handle_API.patch_statuscode(requestBody, endpoint);
		responseBody = Common_Method_Handle_API.post_responseBody(requestBody, endpoint);
		System.out.println("Patch API Response body :" +responseBody);
	}

	@Then("Validate the Patch Status Code")
	public void validate_the_patch_status_code() {
		Assert.assertEquals(StatusCode, 200);
		}

	@Then("Validate Patch Response Body")
	public void validate_patch_response_body() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Patch API : Response Body Validation Successfull");	
		}

}
