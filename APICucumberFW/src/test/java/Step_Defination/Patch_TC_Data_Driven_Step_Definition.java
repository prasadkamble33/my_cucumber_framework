package Step_Defination;

import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.patch_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;

public class Patch_TC_Data_Driven_Step_Definition {
	static String requestBody;
	static int StatusCode;
	static String responseBody;
	static String endpoint;

	@Given("Enter {string} and {string} in Patch request body")
	public void enter_and_in_patch_request_body(String req_name, String req_job) throws IOException {
		endpoint = patch_endpoint.patch_endpoint_TC3();
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n"+ "}";
//	    throw new io.cucumber.java.PendingException();
	}
	
	@When("Send request with valid Patch input data")
	public void send_request_with_valid_patch_input_data() {
		StatusCode = Common_Method_Handle_API.patch_statuscode(requestBody, endpoint);
		responseBody = Common_Method_Handle_API.post_responseBody(requestBody, endpoint);
		System.out.println("Patch API Response body :" +responseBody);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate the data driven Patch Status Code")
	public void validate_the_data_driven_patch_status_code() {
		Assert.assertEquals(StatusCode, 200);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate data driven Patch Response Body")
	public void validate_data_driven_patch_response_body() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Patch API : Response Body Validation Successfull");
//	    throw new io.cucumber.java.PendingException();
	}
}
