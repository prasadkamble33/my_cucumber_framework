package Step_Defination;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;

import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.post_endpoint;

import static io.restassured.RestAssured.given;

import java.io.IOException;

public class Post_Step_Defination extends Common_Method_Handle_API  {
	
	String requestBody;
	int statuscode;
	String responseBody;
	String endpoint;
	
	@Before
	public void Test_Setup() {
		System.out.println("API Triggering");
	}
	
	@Given("Enter NAME and JOB in Post request body")
	public void enter_name_and_job_in_Post_request_body() throws IOException {
		endpoint = post_endpoint.post_endpoint_TC1();
		requestBody = Post_request_repository.post_request_TC1();
//	    throw new io.cucumber.java.PendingException();
	}
		
	@When("Send the requestwith Post payload")
	public void send_the_requestwith_Post_payload() {
		statuscode = Common_Method_Handle_API.post_statuscode(requestBody, endpoint);
		responseBody = Common_Method_Handle_API.post_responseBody(requestBody, endpoint);
		System.out.println("Post API Response body :" +responseBody);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate Post status code")
	public void validate_Post_status_code() {
	    Assert.assertEquals(statuscode, 201);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate Post response body")
	public void validate_Post_response_body() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name=jspresponse.getString("name");
		String res_job=jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Post API : Response Body Validation Successfull");	    
//		throw new io.cucumber.java.PendingException();
	}
	
	@After
	public void Tear_Down() {
		System.out.println("API Proccess end");
	}


}
