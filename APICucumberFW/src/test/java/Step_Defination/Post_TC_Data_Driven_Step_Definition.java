package Step_Defination;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Post_TC_Data_Driven_Step_Definition {
	
	String requestBody;
	int statuscode;
	String responseBody;

	@Given("Enter {string} and {string} in Post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
//	    throw new io.cucumber.java.PendingException();
		}
		
	@When("Send the request with Post input data")
	public void send_the_request_with_post_input_data() {
		statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.post("/api/users").then().extract().response().asString();
		System.out.println("Post API Response body :" +responseBody);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate data driven Post status code")
	public void validate_data_driven_post_status_code() {
		 Assert.assertEquals(statuscode, 201);
//	    throw new io.cucumber.java.PendingException();
	}
	
	@Then("Validate data driven Post response body")
	public void validate_data_driven_post_response_body() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name=jspresponse.getString("name");
		String res_job=jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);	
		System.out.println("Post API : Response Body Validation Successfull");
//	    throw new io.cucumber.java.PendingException();
	}
	
}
