package Step_Defination;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;
import restassured_referance.Put_referance;

public class Put_Step_Defination extends Common_Method_Handle_API {
	static String requestBody;
	static int Statuscode;
	static String responseBody;
	static String endpoint;

	@Given("Input NAME and JOB in Put request body")
	public void input_name_and_job_in_put_request_body() throws IOException {
		endpoint = put_endpoint.put_endpoint_TC2();
		requestBody = Put_request_repository.put_request_TC2();
	}

	@When("Send request with valid Put payload")
	public void send_request_with_valid_put_payload() {
		Statuscode = Common_Method_Handle_API.put_statuscode(requestBody, endpoint);
		responseBody = Common_Method_Handle_API.put_responseBody(requestBody, endpoint);
		System.out.println("Put API Response body :" + responseBody);
	}

	@Then("Validate the Put Status Code")
	public void validate_the_put_status_code() {
		Assert.assertEquals(Statuscode, 200);
	}

	@Then("Validate Put Response Body")
	public void validate_put_response_body() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Put API : Response Body Validation Successfull");
	}

}
