package Step_Defination;

import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_Method_Handle_API;
import Endpoint.put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;

public class Put_TC_Data_Driven_Step_Definition {
	
	static String requestBody;
	static int Statuscode;
	static String responseBody;
	static String endpoint;

	@Given("Input {string} and {string} in Put request body")
	public void input_and_in_put_request_body(String req_name, String req_job) throws IOException {
		endpoint = put_endpoint.put_endpoint_TC2();
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
//	    throw new io.cucumber.java.PendingException();
	}
	@When("Send request with valid Put input data")
	public void send_request_with_valid_put_input_data() {
		Statuscode = Common_Method_Handle_API.put_statuscode(requestBody, endpoint);
		responseBody = Common_Method_Handle_API.put_responseBody(requestBody, endpoint);
		System.out.println("Put API Response body :" + responseBody);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("Validate the data driven Put Status Code")
	public void validate_the_data_driven_put_status_code() {
		Assert.assertEquals(Statuscode, 200);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data driven Put Response Body")
	public void validate_data_driven_put_response_body() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Put API : Response Body Validation Successfull");
//	    throw new io.cucumber.java.PendingException();
	}
	
}
