package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = {"Step_Defination"}, tags= "@Post_API_TestScript or @Put_API_TestScript or @Patch_API_TestScript")
public class Test_Runner {
	
}
 