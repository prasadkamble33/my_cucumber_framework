package restassured_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_referance {

	public static void main(String[] args) {

		// step 1 - Declare the BaseURL
		RestAssured.baseURI = "https://reqres.in/";

		// step 2 - Configure request parameter and trigger the API
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		String responsebody = given().header("Content-Type", "application/json")
				.body("{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}").log().all()
				.when().post("api/users").then().log().all().extract().response().asString();
		// System.out.println("responsebody is :" + responsebody);

		// Step 3 - Create an object of JsonPath to parse the request body and then
		// response body
		JsonPath jsp_res = new JsonPath(responsebody);
		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);

		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);

		String res_id = jsp_res.getString("id");
		System.out.println("id is :" + res_id);

		String res_createddate = jsp_res.getString("createdAt");
		res_createddate = res_createddate.substring(0, 11);
		System.out.println("createddate is :" + res_createddate);

		// Step 4 - Validate responsebody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createddate, expecteddate);

	}

}
