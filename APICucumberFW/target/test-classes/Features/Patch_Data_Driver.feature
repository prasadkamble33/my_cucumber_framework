Feature: Trigger Patch API

Scenario Outline: Trigger Patch API request with valid request parameters
    Given Enter "<NAME>" and "<JOB>" in Patch request body
     When Send request with valid Patch input data
     Then Validate the data driven Patch Status Code
     Then Validate data driven Patch Response Body
     
Examples: 
     |NAME|JOB|
     |Pratik|TL|
     |Prasan|Dev|
     |Yogesh|Dev|