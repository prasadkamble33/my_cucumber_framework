Feature: Trigger Post API

Scenario Outline: Trigger Post API request with valid request parameters
    Given Enter "<NAME>" and "<JOB>" in Post request body
     When Send the request with Post input data
     Then Validate data driven Post status code 
     Then Validate data driven Post response body  
     
Examples: 
      |NAME|JOB|
      |Prasad|SrMg|
      |Akshay|TL|
      |Balaji|QA|