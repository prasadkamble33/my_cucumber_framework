Feature: Trigger Put API

Scenario Outline: Trigger Put API request with valid request parameters
    Given Input "<NAME>" and "<JOB>" in Put request body
     When Send request with valid Put input data
     Then Validate the data driven Put Status Code
     Then Validate data driven Put Response Body
     
Examples: 
     |NAME|JOB|
     |Tushar|SrMg|
     |Amar|TL|
     |Madan|Admin|