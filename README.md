# My_Cucumber_Framework

#BDD Cucumber Framework

BDD stand for Behavior Driven Development. BDD is nothing but Software development methodology to share information among the stakeholder in plain text english language and easy understand format which non-technical person can understand.

Gherkin is specific domain language easy to understand which is written in plain english text.

Cucumber is easy to understand format to developed BDD framework. It helps to map plain english text written in Gherkin with a functional java code. 

In my current framework consist of many components: -
1.	Features file.
2.	Cucumber.options.
3.	Step Definitions.
4.	Test Script package.
5.	Common Functions 
    5.1.API related common functions
    5.2.Utilities related common functions
6.	Data file
7.	Libraries 
8.	Report mechanism.

 features file has extension of .feature and always start with feature keyword. I can write multiple test scenarios with help of keywords such as Given, When, Then, And, Scenario, Examples.

In Step Definition consist of method define with help of @Given, @When, @Then annotation. Every method in Step Definition is map with the step define in scenario of feature file.

In drive mechanism I am using test runner class where I executed my test cases using @RunWith and @CucumberOptions where I provided the location of feature file and step definition file to run my test case as Junit test.

In test script package I have all test case with specific test class and I have multiple test cases for each API and for each test case I have one single test class.

In common functions divided into two categories: -
1.API related common functions: -
     This is used for trigger API and fetch the status code and response body.
2. Utility related common functions: -
      1st Utility related common function is use to create API into text file that mean API executed corresponding to endpoint, request body and response body for each API.
      2nd Utility related common function is use to create directory test executed automatically if it’s does not exist then create and if it’s does exist then deleted and create again.
      3rd Utility related common function is use to read data from excel file.
This is my complete common functions.

I can read data from excel file by using Apache poi libraries. I created utility for same.

In libraries which are using into my framework are rest assured to trigger API and extract response status code and extract response body.

By using Json path I can parse response body and then using testNG libraries we are validating response body parameter by using Assert.

Apart of that we can read and write data from excel file by using Apache poi libraries.

Then in reporting mechanism cucumber has its own in-built report index.html and we can generate more interactive report using plugin attribute in test runner class and generate pretty, Json, html, xml reports. And if required we can generate extent report as well using ItestListener interface and listener tag in testNG xml.



